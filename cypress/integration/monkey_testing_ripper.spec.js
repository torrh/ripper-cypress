describe('Los estudiantes under monkeys', function() {
    it('visits los estudiantes and survives monkeys', function() {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.wait(1000);
        randomLink(10);
        cy.wait(2000);
        randomEvent(10);
    })
})

function randomLink(monkeysLeft) {

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };

    var monkeysLeft = monkeysLeft;
    if(monkeysLeft > 0) {
        cy.get('a').then($links => {
            var randomLink = $links.get(getRandomInt(0, $links.length));
            cy.wrap(randomLink).click({force: true});
            monkeysLeft = monkeysLeft - 1;
            setTimeout(randomLink, 1000, monkeysLeft);
        });
    }
}

function randomEvent(monkeysLeft) {

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };

    var monkeysLeft = monkeysLeft;
    if(monkeysLeft > 0) {

      cy.get('button').then($el => {
         var randomButton = $el.get(getRandomInt(0, $el.length));
         cy.wrap(randomButton).click({force: true});
      });

      cy.wait(2000);

      cy.get('input').then($input => {
         var randomInput = $input.get(getRandomInt(0, $input.length));
         cy.wrap(randomInput).click({force: true}).type("Texto87432834789273492743");
      });

      cy.wait(2000);

      cy.get('select').then($select => {
         var randomSelect = $select.get(getRandomInt(0, $select.length));
         cy.wrap(randomSelect).select('Universidad de los Andes', {force: true})
      });

      monkeysLeft = monkeysLeft - 1;
      setTimeout(randomEvent, 1000, monkeysLeft);

    }
}
